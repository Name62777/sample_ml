import pandas as pd
from sklearn.tree import DecisionTreeClassifier
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score

music_data = pd.read_csv('music.csv')

X  = music_data.drop(columns=['genre'])
Y = music_data['genre']

X_tarin ,  X_test, Y_tarin , Y_test  = train_test_split(X,Y,test_size=0.2)

model = DecisionTreeClassifier()

model.fit(X_tarin,Y_tarin)

preds = model.predict(X_test)

accuracy = accuracy_score(Y_test,preds)

print(accuracy)